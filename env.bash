SECRETS_BASE_PATH=$(pwd)/secrets
GOOGLE_APPLICATION_CREDENTIALS_PATH="$SECRETS_BASE_PATH/gcp/key.json"

export PROJECT_ID=konga-274018
export REGION=europe-west4
export CLUSTER_NAME=kong-cluster

if [[ -f "$GOOGLE_APPLICATION_CREDENTIALS_PATH" ]]; then
  export GOOGLE_APPLICATION_CREDENTIALS="$GOOGLE_APPLICATION_CREDENTIALS_PATH"
else
  echo "[WARN] I couldn't find the key.json file."
fi
