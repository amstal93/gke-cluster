# KONGAHQ GKE Cluster Terraform Deployment

[![pipeline status](https://gitlab.com/kongahq/gke-cluster/badges/master/pipeline.svg)](https://gitlab.com/kongahq/gke-cluster/-/commits/master)

A primary GKE cluster **Terraform** deployment. The setup's pretty straight forward, and it's meant for development purposes in the scope of KONGAHQ only, with the default settings.

The deployment itself is highly customizable, the base set of variables available in the [variables.tf](./variables.tf) file.

With the current values, the GKE cluster gets the following nodes' settings:

```hlc
# terraform.tfvars

node_pools = [
  {
    name                       = "default"
    initial_node_count         = 1
    autoscaling_min_node_count = 2
    autoscaling_max_node_count = 3
    management_auto_upgrade    = true
    management_auto_repair     = true
    node_config_machine_type   = "n1-standard-4"
    node_config_disk_type      = "pd-standard"
    node_config_disk_size_gb   = 100
    node_config_preemptible    = false
  },
]

# ... rest of the file's body
```

## How to use it

To facilitate the **kubectl** project's setup, the deployment, and correctly refer to the remote backend, leverage the [**deploy.bash**](./deploy.bash) script, it does the heavy lifting for you.
Before running the script, check the environent setup in the file [**env.bash**](./env.bash), and modify it accordingly, if necessary:

```bash
# env.bash

SECRETS_BASE_PATH=$(pwd)/secrets
GOOGLE_APPLICATION_CREDENTIALS_PATH="$SECRETS_BASE_PATH/gcp/key.json"

export PROJECT_ID=konga-274018
export REGION=europe-west4

if [[ -f "$GOOGLE_APPLICATION_CREDENTIALS_PATH" ]]; then
  export GOOGLE_APPLICATION_CREDENTIALS="$GOOGLE_APPLICATION_CREDENTIALS_PATH"
fi
```

The script creates also the main namespace **kong** and claims two storage classes, regional, for highly available setups:

```yaml
---
kind: StorageClass
apiVersion: storage.k8s.io/v1
metadata:
  name: regionalpd-storageclass
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-standard
  replication-type: regional-pd
  zones: europe-west4-a, europe-west4-b, europe-west4-c
```

and a high-speed SSD backed class:

```yaml
---
apiVersion: storage.k8s.io/v1
kind: StorageClass
metadata:
  name: ssd-storageclass
provisioner: kubernetes.io/gce-pd
parameters:
  type: pd-ssd
```

## License

[GNU General Public License v3 (GPLv3)](https://gitlab.com/hyperd/piphyperd/blob/master/LICENSE)

## Author Information

[Francesco Cosentino](https://www.linkedin.com/in/francesco-cosentino/)

I'm a surfer, a crypto trader, and a DevSecOps Engineer with 15 years of experience designing highly-available distributed production environments and developing cloud-native apps in public and private clouds.
